#!/bin/bash
#-x 
# add -x to get debugging
#projectName="testprog"
#mainClass=it.lundstedt.erik.Main
#mainClassPath="it/lundstedt/erik/"


clawsPath=./claws
for claw in $clawsPath/*.claw; do
  source $claw
done


manifestDir=META-INF
manifestFile=$manifestDir/MANIFEST.mf

manifest="Manifest-version: 1.0
Main-Class: it.lundstedt.erik.Main
Class-Path: lib/openMenuLib.jar
"
devMode=false
hasDeps=true


loadConf


jarName="$projectName.jar"
DIR="$( dirname $(readlink -f $0) )"

installDestination="$HOME/binOfJars"
installLib=$installDestination/lib

buildDestination="./builds"
buildLib="$buildDestination/lib"

libFolder="./lib"


alias javac="javac -verbose"



function debug()
{
if $devMode ; then
  echo $@
fi
}
function help()
{
echo "nothing here yet"
}
function installLib()
{
  [ -f $installLib ] && debug "$installLib exists!1"
  if [ ! -f "$installLib" ] ; then
    mkdir -p  $installLib
  fi
  [ -f $installLib ] && debug "$installLib exists!1"
  if [ ! -f $installLib ]; then
    debug $installLib
     cp $libFolder/*.jar $installLib
   fi
}
function init()
{
	mkdir "$buildDestination"
  mkdir "$buildLib"
if $hasDeps; then
  sleep 0.5
  cp lib/*.jar $buildLib
fi

}
function clean()
{
	rm -dr "$buildDestination"
}
function compile()
{
  pwd

if $hasDeps; then
  # code
  javac -d $buildDestination src/$mainClassPath*.java -cp lib/*.jar
else
  
  javac -d $buildDestination src/$mainClassPath*.java
fi

}
function package()
{
  (
    cd $buildDestination || exit
    #Class-Path: lib/commons-lang3-3.0.1.jar lib/commons-io-2.1.jar\n 
    mkdir "$manifestDir"
    printf "$manifest">$manifestFile
    jar -cvfm $jarName $manifestFile  $mainClassPath # ../$libFolder/*.jar
  )
  cp  $libFolder/* $buildLib/
}
function runnable()
{
(
	cd $buildDestination||exit
	chmod a+x  "$jarName"
)
}

function install()
{

  (
	  cd $buildDestination||exit
	  cp $jarName $installDestination
	  echo  
	  ls $installDestination
  )
   echo
  (
    cd $installDestination||exit

    #echo "$runScript"
    #echo "$runScript">$projectName
    ln -s $jarName $projectName
    chmod a+x $projectName
   )
}
function uninstall()
{
  (
    cd $installDestination||exit
    rm $jarName
    rm $projectName
    ls
    ls|grep $jarName
    ls|grep $projectName
  )
}

function reinstall()
{
  uninstall
  sleep 2
  install
}

function cleanup()
{
  clean
  init
}

function setup
{
  cleanup
  installLib
  compile
  package
  reinstall
}

function testing()
{
#  echo "this does nothing without crow.claw installed"
  run
 # execute
 # compare
}

function main()
{
setup
#testing
echo $hasDeps

if $hasDeps; then
  # code
  echo "dependencies loaded"
else
  echo "no dependencies to load"
fi


}

function bc()
{
backupScript
}

if
  declare -f
  "$1" > /dev/null
then
  # call arguments verbatim
  "$@"
else
   main
  
#  echo $projectName
#  echo $mainClass
#  echo $mainClassPath

  sleep 1
  echo "DONE!"
  echo "press enter to exit";read 



  
fi


# Local Variables:
# eval: (exec-path-from-shell-initialize)
# End:

