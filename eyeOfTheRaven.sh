#!/bin/env bash

awk -F '::' '{print $1}' targets.info|fzf --preview "grep -m 1 {} targets.info"
